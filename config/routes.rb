Rails.application.routes.draw do
  resources :answers
  resources :questions
  resources :categories
  resources :quizzes

  get "up" => "rails/health#show", as: :rails_health_check

  # Defines the root path route ("/")
  # root "posts#index"
end
