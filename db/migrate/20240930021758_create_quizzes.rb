class CreateQuizzes < ActiveRecord::Migration[7.2]
  def change
    create_table :quizzes do |t|
      t.string :title, null: false
      t.integer :type, default: 0
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
