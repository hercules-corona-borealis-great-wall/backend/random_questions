class CreateAnswers < ActiveRecord::Migration[7.2]
  def change
    create_table :answers do |t|
      t.text :value
      t.belongs_to :answer_type, null: false, foreign_key: true

      t.timestamps
    end
  end
end
