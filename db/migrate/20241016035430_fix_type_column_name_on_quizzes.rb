class FixTypeColumnNameOnQuizzes < ActiveRecord::Migration[7.2]
  def change
    rename_column :quizzes, :type, :kind
  end
end
