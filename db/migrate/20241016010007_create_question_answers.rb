class CreateQuestionAnswers < ActiveRecord::Migration[7.2]
  def change
    create_table :question_answers do |t|
      t.belongs_to :question, null: false, foreign_key: true
      t.belongs_to :answer, null: false, foreign_key: true
      t.boolean :correct_answer, default: false

      t.timestamps
    end
  end
end
