class CreateCategories < ActiveRecord::Migration[7.2]
  def change
    create_table :categories do |t|
      t.string :name
      t.belongs_to :quiz, null: false, foreign_key: true

      t.timestamps
    end
  end
end
