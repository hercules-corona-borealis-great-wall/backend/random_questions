require 'rails_helper'

RSpec.describe Answer, type: :model do
  let(:answer_type){ FactoryBot.create(:answer_type) }
  let(:valid_answer){ FactoryBot.build(:answer, value: 'Question #1', answer_type_id: answer_type.id) }
  let(:invalid_answer) { FactoryBot.build(:answer, value: nil, answer_type_id: nil) }

  context 'Basic model validations' do
    it 'Should be valid' do
      expect(valid_answer).to be_valid
      expect(valid_answer.save).to eq(true)
      expect(valid_answer.answer_type_id.present?).to eq(true)
      expect(valid_answer.answer_type.name).to eq(answer_type.name)
    end

    it 'Should not be valid' do
      expect(invalid_answer).to be_invalid
      expect(invalid_answer.save).to eq(false)
      expect(invalid_answer.answer_type_id.present?).to eq(false)
    end
  end
end
