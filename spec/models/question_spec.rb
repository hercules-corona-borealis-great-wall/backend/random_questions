require 'rails_helper'

RSpec.describe Question, type: :model do
  let(:answer_type){ FactoryBot.create(:answer_type) }
  let(:quiz){ FactoryBot.create(:quiz, status: :in_draft, kind: :collective) }
  let(:category){ FactoryBot.create(:category, quiz_id: quiz.id) }
  let(:valid_question){ FactoryBot.build(:question, category_id: category.id) }
  let(:invalid_question){ FactoryBot.build(:question, category_id: nil) }
  let(:answers){ FactoryBot.create_list(:answer, 5, answer_type_id: answer_type.id) }

  context 'Model basic validations' do
    it 'Should be valid' do
      quiz.update!(status: :published)
      expect(valid_question.category.quiz.status != :in_draft).to eq(true)
      expect(valid_question).to be_valid
      expect(valid_question.save).to eq(true)
      expect(valid_question.id.present?).to eq(true)
      expect(valid_question.errors.present?).to eq(false)
    end

    it 'Should be invalid' do
      expect(invalid_question).to be_invalid
      expect(invalid_question.save).to eq(false)
      expect(invalid_question.errors.present?).to eq(true)
    end
  end
end
