require 'rails_helper'

RSpec.describe Quiz, type: :model do
  let(:valid_quiz){ FactoryBot.build(:quiz) }
  let(:invalid_quiz){ FactoryBot.build(:quiz, status: 'in_progress') }
  let(:categories){ FactoryBot.build_list(:category, 5) }

  context 'Basic model validations' do
    it 'Should be valid' do
      expect(valid_quiz).to be_valid
      expect(valid_quiz.save).to eq(true)
      expect(valid_quiz.errors.present?).to eq(false)
      expect(valid_quiz.title.present?).to eq(true)
      expect(valid_quiz.status.to_sym).to eq(:in_draft)
      expect(valid_quiz.kind.to_sym).to eq(:exclusive)
      expect(valid_quiz.can_be_answered).to eq(false)

      categories.each do |category|
        category.quiz_id = valid_quiz.id
        category.save
      end

      expect(categories.pluck(:quiz_id).uniq.include?(valid_quiz.id)).to eq(true)
    end

    it 'Should be invalid' do
      expect(invalid_quiz).to be_invalid
      expect(invalid_quiz.save).to eq(false)
      expect(invalid_quiz.errors.present?).to eq(true)
      expect(invalid_quiz.errors.count).to eq(1)
    end
  end
end
