require 'rails_helper'

RSpec.describe QuestionAnswer, type: :model do
  let(:quiz){ FactoryBot.create(:quiz) }
  let(:category){ FactoryBot.create(:category, quiz_id: quiz.id) }
  let(:question){ FactoryBot.create(:question, category_id: category.id) }
  let(:answer_type){ FactoryBot.create(:answer_type) }
  let(:answer){ FactoryBot.create(:answer, answer_type_id: answer_type.id) }
  let(:valid_question_answer){ FactoryBot.build(:question_answer, question_id: question.id, answer_id: answer.id) }
  let(:invalid_question_answer){ FactoryBot.build(:question_answer) }

  context 'Basic model validations' do
    it 'Should be valid' do
      expect(valid_question_answer).to be_valid
      expect(valid_question_answer.save).to eq(true)
      expect(valid_question_answer.id.present?).to eq(true)
      expect(valid_question_answer.errors.present?).to eq(false)
      expect(valid_question_answer.question_id).to eq(question.id)
      expect(valid_question_answer.answer_id).to eq(answer.id)
    end
    
    it 'Should not be valid' do
      expect(invalid_question_answer).to be_invalid
      expect(invalid_question_answer.save).to eq(false)
      expect(invalid_question_answer.errors.present?).to eq(true)
    end
  end
end
