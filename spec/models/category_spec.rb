require 'rails_helper'

RSpec.describe Category, type: :model do
  let(:quiz){ FactoryBot.create(:quiz, title: "My test quiz") }
  let(:valid_category){ FactoryBot.build(:category, quiz_id: quiz.id) }
  let(:invalid_category){ FactoryBot.build(:category, name: nil, quiz_id: nil) }

  context 'Basic model validations' do
    it 'Should be valid category' do
      expect(valid_category).to be_valid
      expect(valid_category.save).to eq(true)
      expect(valid_category.id.present?).to eq(true)
      expect(valid_category.quiz_id).to eq(quiz.id)
      expect(valid_category.quiz.title).to eq(quiz.title)
    end

    it 'Should be an invalid category' do
      expect(invalid_category).to be_invalid
      expect(invalid_category.save).to eq(false)
      expect(invalid_category.id.present?).to eq(false)
      expect(invalid_category.errors.present?).to eq(true)
    end
  end
end
