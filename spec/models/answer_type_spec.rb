require 'rails_helper'

RSpec.describe AnswerType, type: :model do
  let(:valid_answer_type) { FactoryBot.build(:answer_type) }
  let(:invalid_answer_type) { FactoryBot.build(:answer_type, name: nil)}

  context 'Basic Model validations' do
    it 'Should be a valid answer_type' do
      expect(valid_answer_type).to be_valid
      expect(valid_answer_type.save).to eq(true)
    end

    it 'Should be an invalid answer_type' do
      expect(invalid_answer_type).to be_invalid
      expect(invalid_answer_type.save).to eq(false)
      expect(invalid_answer_type.errors.count).to eq(1)
    end
  end
end
