require 'rails_helper'

RSpec.describe "/questions", type: :request do
  let(:valid_attributes) {
    skip("Add a hash of attributes valid for your model")
  }

  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

  let(:valid_headers) {
    {}
  }

  describe "GET /index" do
    it "renders a successful response" do
      Question.create! valid_attributes
      get questions_url, headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe "GET /show" do
    it "renders a successful response" do
      question = Question.create! valid_attributes
      get question_url(question), as: :json
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Question" do
        expect {
          post questions_url,
               params: { question: valid_attributes }, headers: valid_headers, as: :json
        }.to change(Question, :count).by(1)
      end

      it "renders a JSON response with the new question" do
        post questions_url,
             params: { question: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end

    context "with invalid parameters" do
      it "does not create a new Question" do
        expect {
          post questions_url,
               params: { question: invalid_attributes }, as: :json
        }.to change(Question, :count).by(0)
      end

      it "renders a JSON response with errors for the new question" do
        post questions_url,
             params: { question: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end
  end

  describe "PATCH /update" do
    context "with valid parameters" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested question" do
        question = Question.create! valid_attributes
        patch question_url(question),
              params: { question: new_attributes }, headers: valid_headers, as: :json
        question.reload
        skip("Add assertions for updated state")
      end

      it "renders a JSON response with the question" do
        question = Question.create! valid_attributes
        patch question_url(question),
              params: { question: new_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end

    context "with invalid parameters" do
      it "renders a JSON response with errors for the question" do
        question = Question.create! valid_attributes
        patch question_url(question),
              params: { question: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end
  end

  describe "DELETE /destroy" do
    it "destroys the requested question" do
      question = Question.create! valid_attributes
      expect {
        delete question_url(question), headers: valid_headers, as: :json
      }.to change(Question, :count).by(-1)
    end
  end
end
