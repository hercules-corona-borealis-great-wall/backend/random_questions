require 'rails_helper'

RSpec.describe "/quizzes", type: :request do
  let(:valid_attributes) {
    { title: "My first quiz" }
  }

  let(:valid_headers) {
    {}
  }

  describe "GET /index" do
    it "renders a successful response" do
      Quiz.create!(valid_attributes)

      get(quizzes_url, headers: valid_headers, as: :json)
      expect(response).to be_successful
      expect(response.parsed_body.present?).to eq(true)

      quiz = response.parsed_body.first

      expect(quiz[:kind].to_sym).to eq(:exclusive)
      expect(quiz[:status].to_sym).to eq(:in_draft)
    end
  end

  describe "GET /show" do
    it "renders a successful response" do
      Quiz.create!(title: "Draft Quizz")

      quiz = Quiz.create!(valid_attributes)
      get(quiz_url(quiz), as: :json)

      expect(response).to be_successful
      expect(response.parsed_body[:id]).to eq(quiz.id)
      expect(response.parsed_body[:title]).to eq(quiz.title)
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Quiz" do
        expect {
          post(quizzes_url, params: { quiz: valid_attributes }, headers: valid_headers, as: :json)
        }.to change(Quiz, :count).by(1)
      end

      it "renders a JSON response with the new quiz" do
        post(quizzes_url, params: { quiz: valid_attributes }, headers: valid_headers, as: :json)

        expect(response).to have_http_status(:created)
        expect(response.parsed_body[:id].present?).to eq(true)
        expect(response.parsed_body[:status].to_sym).to eq(:in_draft)
        expect(response.parsed_body[:kind].to_sym).to eq(:exclusive)
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) { { title: 'New title' } }

      it 'Passing quiz to published' do
        quiz = Quiz.create!(valid_attributes)
        old_updated_at = quiz.updated_at
        old_title = quiz.title

        patch(
          quiz_url(quiz),
          params: { quiz: new_attributes.merge({status: 'published'}) },
          headers: valid_headers,
          as: :json
        )

        quiz.reload

        expect(response).to be_successful
        expect(quiz.updated_at).not_to eq(old_updated_at)
        expect(quiz.title).not_to eq(old_title)
        expect(quiz.status.to_sym).not_to eq(:in_draft)
        expect(quiz.status.to_sym).to eq(:published)
        expect(quiz.can_be_answered).to eq(true)
      end

      it 'Passing quiz to in_progress' do
        quiz = Quiz.create!(valid_attributes)
        old_updated_at = quiz.updated_at

        patch(
          quiz_url(quiz),
          params: { quiz: new_attributes.merge({status: 'in_progress'}) },
          headers: valid_headers,
          as: :json
        )

        quiz.reload

        expect(response).to be_successful
        expect(quiz.updated_at).not_to eq(old_updated_at)
        expect(quiz.status.to_sym).not_to eq(:in_draft)
        expect(quiz.status.to_sym).to eq(:in_progress)
        expect(quiz.can_be_answered).to eq(true)
      end

      it 'Passing quiz to completed' do
        quiz = Quiz.create!(valid_attributes)
        old_updated_at = quiz.updated_at

        patch(
          quiz_url(quiz),
          params: { quiz: new_attributes.merge({status: 'completed'}) },
          headers: valid_headers,
          as: :json
        )

        quiz.reload

        expect(response).to be_successful
        expect(quiz.updated_at).not_to eq(old_updated_at)
        expect(quiz.status.to_sym).not_to eq(:in_draft)
        expect(quiz.status.to_sym).to eq(:completed)
        expect(quiz.can_be_answered).to eq(false)
      end

      it 'Passing quiz to in_draft' do
        quiz = Quiz.create!(valid_attributes.merge({status: 'in_draft'}))
        quiz.update!(status: :published)
        old_updated_at = quiz.updated_at

        patch(
          quiz_url(quiz),
          params: { quiz: new_attributes.merge({status: 'in_draft'}) },
          headers: valid_headers,
          as: :json
        )

        quiz.reload

        expect(response).to be_successful
        expect(quiz.updated_at).not_to eq(old_updated_at)
        expect(quiz.status.to_sym).not_to eq(:published)
        expect(quiz.status.to_sym).to eq(:in_draft)
        expect(quiz.can_be_answered).to eq(false)
      end
    end

    context "with invalid parameters" do
      let(:invalid_attributes){ { title:nil } }

      it "renders a JSON response with errors for the quiz" do
        quiz = Quiz.create!(valid_attributes)

        patch(
          quiz_url(quiz),
          params: { quiz: invalid_attributes },
          headers: valid_headers,
          as: :json
        )

        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "DELETE /destroy" do
    it "destroys the requested quiz" do
      quiz = Quiz.create!(valid_attributes)

      expect {
        delete(quiz_url(quiz), headers: valid_headers, as: :json)
      }.to change(Quiz, :count).by(-1)
    end
  end
end
