FactoryBot.define do
  factory :category do
    name { "Default category" }
    quiz_id { nil }
  end
end
