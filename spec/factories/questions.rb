FactoryBot.define do
  factory :question do
    value { "MyText" }
    category_id { nil }
  end
end
