FactoryBot.define do
  factory :question_answer do
    question_id { nil }
    answer_id { nil }
    correct_answer { false }
  end
end
