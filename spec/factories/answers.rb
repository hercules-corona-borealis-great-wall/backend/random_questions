FactoryBot.define do
  factory :answer do
    value { "This is a stantard answer" }
    answer_type_id { nil }
  end
end
