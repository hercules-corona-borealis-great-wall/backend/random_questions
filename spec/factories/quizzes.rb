FactoryBot.define do
  factory :quiz do
    title { "My default quiz" }
    kind { :exclusive }
    status { :in_draft }
  end
end
