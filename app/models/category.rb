class Category < ApplicationRecord
  has_many :questions

  belongs_to :quiz

  after_initialize :set_defaults

  validates :quiz_id, presence: true
  validates :name, presence: true

  def set_defaults
    self.name ||= 'Default Category'
  end
end
