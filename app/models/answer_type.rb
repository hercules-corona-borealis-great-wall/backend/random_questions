class AnswerType < ApplicationRecord
  has_many :answers

  validates :name, presence: true

end
