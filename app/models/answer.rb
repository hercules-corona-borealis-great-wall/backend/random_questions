class Answer < ApplicationRecord
  has_many :question_answers
  has_many :questions, through: :question_answers

  belongs_to :answer_type

  validates :value, presence: true
  validates :answer_type_id, presence: true
end
