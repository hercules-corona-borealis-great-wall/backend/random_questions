class Quiz < ApplicationRecord
  has_many :categories

  after_initialize :set_defaults

  validates :title, presence: true
  validate :validate_initial_status

  enum :status, [ :in_draft, :published, :in_progress, :completed ]
  enum :kind, [ :exclusive, :collective ]

  def set_defaults
    self.title ||= "Quizz default title"
    self.status ||= :in_draft
    self.kind ||= :exclusive
  end

  def can_be_answered
    [:published, :in_progress].include?(status.to_sym)
  end

  def validate_initial_status
    return if id.present?

    set_defaults

    unless status.to_sym.eql?(:in_draft)
      errors.add(:status, I18n.t('quiz.error.invalid_initial_status'))
    end
  end
end
