class Question < ApplicationRecord
  has_many :question_answers
  has_many :answers, through: :question_answers

  belongs_to :category

  validates :value, presence: true
  validates :category_id, presence: true
end

