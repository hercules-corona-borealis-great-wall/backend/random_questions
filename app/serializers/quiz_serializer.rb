class QuizSerializer < ActiveModel::Serializer
  attributes :id, :title, :kind, :status
end
