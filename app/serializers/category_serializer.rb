class CategorySerializer < ActiveModel::Serializer
  attributes :id, :name
  has_one :quiz_id
end
