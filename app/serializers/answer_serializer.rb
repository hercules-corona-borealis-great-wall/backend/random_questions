class AnswerSerializer < ActiveModel::Serializer
  attributes :id, :value
  has_one :answer_type_id
end
