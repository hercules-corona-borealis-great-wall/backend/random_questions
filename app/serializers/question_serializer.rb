class QuestionSerializer < ActiveModel::Serializer
  attributes :id, :value
  has_one :category_id
end
